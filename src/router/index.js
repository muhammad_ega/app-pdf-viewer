import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/list-pdf",
      name: "list-pdf",
      component: () => import("../views/ListPdf.vue"),
    },
    {
      path: "/view-local",
      name: "view-pdf-local",
      component: () => import("../views/PdfView.vue"),
    },
    {
      path: "/view-from-url",
      name: "view-url-pdf",
      component: () => import("../views/pdfViewUrl.vue"),
    },
  ],
});

export default router;
